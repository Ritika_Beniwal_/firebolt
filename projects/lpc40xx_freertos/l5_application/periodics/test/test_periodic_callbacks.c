#include "unity.h"
#include <stdio.h>
#include <string.h>

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file

// Include the source we wish to test
#include "Mockcan_bus.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_bus_message_handler_rx.h"
#include "Mockcan_bus_message_handler_tx.h"
#include "Mockcan_mia_configuration.h"
#include "Mocklcd_init.h"
#include "Mockprint_lcd_messages.h"
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can__bus_initializer_Expect(can1);
  lcd__init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__10Hz(void) {
  can_bus_handler__process_all_received_messages_Expect(can1);
  can_bus_handler__manage_mia_Expect();
  can_bus_handler_tx_messages_Expect(can1);
  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__1Hz(void) {
  can_bus_handler_tx_debug_messsages_Expect(can1);
  lcd__communication_init_Expect();
  print_info_on_lcd_Expect();
  periodic_callbacks__1Hz(0);
}