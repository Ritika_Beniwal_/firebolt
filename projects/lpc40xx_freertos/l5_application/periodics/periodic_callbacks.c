#include "periodic_callbacks.h"
#include "can_bus.h"
#include "can_bus_initializer.h"
#include "can_bus_message_handler_rx.h"
#include "can_bus_message_handler_tx.h"
#include "head_tail_lights.h"
#include "lcd_init.h"
#include "leds_for_debug_init.h"
#include "print_lcd_messages.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */

void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  can__bus_initializer(can1);
  lcd__init();
  head_tail_lights_init();
  debug_led_init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  // Add your code here
  can_bus_handler_tx_debug_messsages(can1);
  static uint8_t count = 0;
  if (count == 0) {
    lcd__communication_init();
  }
  count = 1;
  print_info_on_lcd();
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // Add your code here
  can_bus_handler__process_all_received_messages(can1);
  can_bus_handler__manage_mia();
  can_bus_handler_tx_messages(can1);
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  // Add your code here
  if (callback_count % 2 == 0) {
  }
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // Add your code here
}