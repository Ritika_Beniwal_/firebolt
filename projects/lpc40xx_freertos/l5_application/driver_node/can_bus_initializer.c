#include "can_bus_initializer.h"
#include <string.h>

static const uint32_t baudrate_kbps = 100;
static const uint16_t rxq_size = 100;
static const uint16_t txq_size = 100;

void can__bus_initializer(can__num_e can) {
  can__init(can, baudrate_kbps, rxq_size, txq_size, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  
  if(can__is_bus_off(can)){
   can__reset_bus(can);
  }
  
}