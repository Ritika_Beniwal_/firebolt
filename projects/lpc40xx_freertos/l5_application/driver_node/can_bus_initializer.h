#pragma once

#include "can_bus.h"
#include <stdbool.h>

void can__bus_initializer(can__num_e can);
