#include "can_bus_message_handler_rx.h"
#include "board_io.h"
#include "can_bus.h"
#include "can_mia_configuration.h"
#include "driver_logic.h"
#include "leds_for_debug_init.h"
#include "project.h"
#include <stdio.h>

static dbc_SENSOR_SONARS_s sensor_data = {0};
static dbc_GEO_CONTROLLER_COMPASS_s geo_status = {0};
static dbc_MOTOR_SPEED_s rpm_sensor = {0};
static dbc_DRIVE_STATUS_s decoded_start_signal = {.DRIVE_START_STOP = 5};

const static int sensor_mia_threshold = 5;
const static int geo_mia_threshold = 5;

const uint32_t dbc_mia_threshold_SENSOR_SONARS = sensor_mia_threshold;
const dbc_SENSOR_SONARS_s dbc_mia_replacement_SENSOR_SONARS = {};

const uint32_t dbc_mia_threshold_GEO_CONTROLLER_COMPASS = geo_mia_threshold;
const dbc_GEO_CONTROLLER_COMPASS_s dbc_mia_replacement_GEO_CONTROLLER_COMPASS = {};

void can_bus_handler__process_all_received_messages(can__num_e can_num) {
  can__msg_t can_msg = {0};
  int timeout = 0;

  // Receive all messages
  while (can__rx(can_num, &can_msg, timeout)) {
    const dbc_message_header_t header = {.message_id = can_msg.msg_id, .message_dlc = can_msg.frame_fields.data_len};
    // decode sensor data
    if (dbc_decode_SENSOR_SONARS(&sensor_data, header, can_msg.data.bytes) &&
        header.message_id == dbc_header_SENSOR_SONARS.message_id) {
      driver_process_sonar_sensor_input(&sensor_data);
      gpio__toggle(sensor_mia_led);
    }

    // decode geo data
    if (dbc_decode_GEO_CONTROLLER_COMPASS(&geo_status, header, can_msg.data.bytes) &&
        header.message_id == dbc_header_GEO_CONTROLLER_COMPASS.message_id) {
      driver__process_geo_controller_directions(&geo_status);
      gpio__toggle(geo_mia_led);
    }

    // decode rpm sensor value
    if (dbc_decode_MOTOR_SPEED(&rpm_sensor, header, can_msg.data.bytes)) {
      driver__process_rpm_values(&rpm_sensor);
    }

    // decode start signal from bridge sensor
    if (dbc_decode_DRIVE_STATUS(&decoded_start_signal, header, can_msg.data.bytes)) {
      driver__process_start_stop_signal(&decoded_start_signal);
    }
  }
}

void can_bus_handler__manage_mia(void) {
  const uint32_t mia_increment_value = 1;

  if (dbc_service_mia_SENSOR_SONARS(&sensor_data, mia_increment_value)) {
    gpio__set(sensor_mia_led);
  }

  else if (dbc_service_mia_GEO_CONTROLLER_COMPASS(&geo_status, mia_increment_value)) {
    gpio__set(geo_mia_led);
  }
  return;
}