#include "print_lcd_messages.h"
#include "driver_logic.h"
#include "lcd_init.h"
#include <stdio.h>

void print_info_on_lcd() {
  static dbc_MOTOR_SPEED_s car_speed = {.MOTOR_SPEED_info = 0};
  static uint32_t distance_to_destination;
  char car_string[100];
  char distance_string[100];
  char sensor_string[100];

  char heading_bearing_str[100];
  char bearing_str[100];
  static uint16_t heading;
  static uint16_t bearing;
  static uint16_t left, right, middle, rear;
  car_speed.MOTOR_SPEED_info = get_car_speed();
  sprintf(car_string, "Speed: %0.1f mph", car_speed.MOTOR_SPEED_info);
  lcd__send_line(0, car_string);

  distance_to_destination = get_distance_to_dest();
  if (distance_to_destination < 1000) {
    sprintf(distance_string, "Dest Dist: %ld m", distance_to_destination);
    lcd__send_line(1, distance_string);
  } else {
    sprintf(distance_string, "GPS not fixed");
    lcd__send_line(1, distance_string);
  }

  heading = get_heading_from_geo();
  bearing = get_bearing_from_geo();
  sprintf(heading_bearing_str, "H:%d deg, B:%d deg", heading, bearing);
  lcd__send_line(2, heading_bearing_str);

  dbc_SENSOR_SONARS_s sensor_data_lcd = get_sensor_values_for_lcd();

  left = sensor_data_lcd.SENSOR_SONARS_left;
  right = sensor_data_lcd.SENSOR_SONARS_right;
  rear = sensor_data_lcd.SENSOR_SONARS_rear;
  middle = sensor_data_lcd.SENSOR_SONARS_middle;

  sprintf(sensor_string, "L:%d R:%d F:%d B:%d\n", left, right, middle, rear);
  lcd__send_line(3, sensor_string);

  // bearing = get_bearing_from_geo();
  // sprintf(bearing_str, "Bearing: %d", bearing);
  // lcd__send_line(3, bearing_str);

  // if (destination_reached()) {
  //   lcd__send_line(3, "Destination Reached");
  // } else {
  //   lcd__send_line(3, "Car Moving");
  // }
  return;
}
