#pragma once
#include "can_bus.h"
#include "driver_logic.h"
#include "project.h"

void can_bus_handler__process_all_received_messages(can__num_e can_num);
void can_bus_handler__manage_mia(void);
