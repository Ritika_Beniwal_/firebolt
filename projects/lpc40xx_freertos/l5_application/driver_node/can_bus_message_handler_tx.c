#include "can_bus_message_handler_tx.h"
#include "board_io.h"
#include "can_bus.h"
#include "driver_logic.h"
#include "gpio.h"
#include "leds_for_debug_init.h"
#include "project.h"
#include <stdio.h>

bool dbc_send_can_message(void *argument, uint32_t message_id, const uint8_t bytes[8], uint8_t dlc) {
  can__msg_t msg = {};

  msg.frame_fields.data_len = dlc;
  msg.msg_id = message_id;
  memcpy(msg.data.bytes, bytes, 8);

  return can__tx(can1, &msg, 0);
}
void can_bus_handler_tx_messages(can__num_e can_num) {

  dbc_DRIVER_TO_MOTOR_s motor_data = driver_logic__get_motor_command();

  if (dbc_encode_and_send_DRIVER_TO_MOTOR(NULL, &motor_data)) {

    gpio__toggle(board_io__get_led0());

    fprintf(stderr, "Spd :%f\n", motor_data.DRIVER_TO_MOTOR_speed);
    fprintf(stderr, "Str:%d\n", motor_data.DRIVER_TO_MOTOR_direction);

  } else {
    gpio__set(can_bus_error_led);
  }
}

void can_bus_handler_tx_debug_messsages(can__num_e can_num) {
  dbc_DRIVER_DEBUG_s driver_debug_data = driver_process_debug_data();
  (void)dbc_encode_and_send_DRIVER_DEBUG(NULL, &driver_debug_data);
}