#pragma once
#include <stdbool.h>
#include <stdint.h>

void lcd__communication_init(void);
void lcd__init(void);
bool lcd__send_line(uint8_t line_number, char *line);
void lcd__reset(void);
void lcd__clear_screen(void);
