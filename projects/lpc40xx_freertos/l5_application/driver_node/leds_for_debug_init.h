#pragma once
#include "gpio.h"

static gpio_s sensor_mia_led, geo_mia_led, can_bus_error_led;

void debug_led_init(void);
void clear_all_leds(void);
