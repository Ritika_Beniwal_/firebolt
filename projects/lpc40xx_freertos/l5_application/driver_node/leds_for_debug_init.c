#include "leds_for_debug_init.h"

void debug_led_init(void) {
  // sensor mia led P0.7
  sensor_mia_led = gpio__construct_as_output(0, 7);
  gpio__set_function(sensor_mia_led, GPIO__FUNCITON_0_IO_PIN);

  // geo mia led P0.6
  geo_mia_led = gpio__construct_as_output(0, 6);
  gpio__set_function(geo_mia_led, GPIO__FUNCITON_0_IO_PIN);

  // can_bus_error_led P1.4
  can_bus_error_led = gpio__construct_as_output(1, 4);
  gpio__set_function(can_bus_error_led, GPIO__FUNCITON_0_IO_PIN);
}

void clear_all_leds(void) {
  gpio__set(sensor_mia_led);
  gpio__set(geo_mia_led);
  gpio__set(can_bus_error_led);
}