#include "Mockcan_bus.h"
#include "can_bus_initializer.h"
#include "unity.h"

void test_can__bus_initializer() {
  can__init_ExpectAndReturn(can1, 100, 100, 100, NULL, NULL, 1);
  can__bypass_filter_accept_all_msgs_Expect();

  can__reset_bus_Expect(can1);

  can__bus_initializer(can1);
}