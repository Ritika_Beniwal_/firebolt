#include "Mockcan_bus.h"
#include "Mockdriver_logic.h"
#include "can_bus_message_handler_tx.h"
#include "project.h"
#include "unity.h"

void test_can_bus_handler_tx_messages_success(void) {
  dbc_DRIVER_TO_MOTOR_s motor_data = {0};
  driver_logic__get_motor_command_ExpectAndReturn(motor_data);

  can__msg_t dummy_msg_tx = {0};
  can__tx_ExpectAndReturn(can1, &dummy_msg_tx, 0, 1);
  can__tx_IgnoreArg_can_message_ptr();
  can_bus_handler_tx_messages(can1);
}

void test_can_bus_handler_tx_messages_failure(void) {
  dbc_DRIVER_TO_MOTOR_s motor_data = {0};
  driver_logic__get_motor_command_ExpectAndReturn(motor_data);

  can__msg_t dummy_msg = {};
  can__tx_ExpectAndReturn(can1, &dummy_msg, 0, 0);
  can__tx_IgnoreArg_can_message_ptr();
  can_bus_handler_tx_messages(can1);
}
