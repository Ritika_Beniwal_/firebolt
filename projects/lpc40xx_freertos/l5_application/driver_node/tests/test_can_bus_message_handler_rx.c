#include "Mockcan_bus.h"
#include "Mockdriver_logic.h"
#include "can_bus_message_handler_rx.h"
#include "project.h"
#include "unity.h"

void driver_process_sonar_sensor_input(dbc_SENSOR_SONARS_s *sensor_data);

// success case when- message id and data length are valid
void test_can_bus_message_handler_rx_usensor_success(void) {
  can__msg_t dummy_msg_rx = {0};
  dbc_SENSOR_SONARS_s sensor_data = {0};
  dbc_message_header_t sensor_header;

  dbc_decode_SENSOR_SONARS(&sensor_data, sensor_header, dummy_msg_rx.data.bytes);
  dummy_msg_rx.msg_id = dbc_header_SENSOR_SONARS.message_id;
  dummy_msg_rx.frame_fields.data_len = dbc_header_SENSOR_SONARS.message_dlc;

  sensor_header.message_id = dummy_msg_rx.msg_id;
  sensor_header.message_dlc = dummy_msg_rx.frame_fields.data_len;

  can__rx_ExpectAndReturn(can1, &dummy_msg_rx, 0, 1);
  can__rx_IgnoreArg_can_message_ptr();

  can__rx_ReturnThruPtr_can_message_ptr(&dummy_msg_rx);
  can__rx_ExpectAndReturn(can1, &dummy_msg_rx, 0, 0);
  can__rx_IgnoreArg_can_message_ptr();

  driver_process_sonar_sensor_input_Expect(&sensor_data);
  can_bus_handler__process_all_received_messages(can1);
}

// incorrect message id & correct data length
void test_can_bus_message_handler_rx_usensor_failure_1(void) {
  can__msg_t dummy_msg_rx = {};
  dbc_SENSOR_SONARS_s sensor_data = {};
  dbc_message_header_t sensor_header;

  dummy_msg_rx.msg_id = dbc_header_SENSOR_SONARS.message_id - 3;
  dummy_msg_rx.frame_fields.data_len = dbc_header_SENSOR_SONARS.message_dlc;

  sensor_header.message_id = dummy_msg_rx.msg_id;
  sensor_header.message_dlc = dummy_msg_rx.frame_fields.data_len;

  dbc_decode_SENSOR_SONARS(&sensor_data, sensor_header, dummy_msg_rx.data.bytes);

  can__rx_ExpectAndReturn(can1, &dummy_msg_rx, 0, 1);
  can__rx_IgnoreArg_can_message_ptr();

  can__rx_ReturnThruPtr_can_message_ptr(&dummy_msg_rx);
  can__rx_ExpectAndReturn(can1, &dummy_msg_rx, 0, 0);
  can__rx_IgnoreArg_can_message_ptr();

  // we will not call driver_process_input function as the msg_id is incorrect
  can_bus_handler__process_all_received_messages(can1);
}

// incorrect data length & correct message id
void test_can_bus_message_handler_rx_usensor_failure_2(void) {
  can__msg_t dummy_msg_rx = {};
  dbc_SENSOR_SONARS_s sensor_data = {};
  dbc_message_header_t sensor_header;

  dummy_msg_rx.msg_id = dbc_header_SENSOR_SONARS.message_id;
  dummy_msg_rx.frame_fields.data_len = dbc_header_SENSOR_SONARS.message_dlc - 3;

  sensor_header.message_id = dummy_msg_rx.msg_id;
  sensor_header.message_dlc = dummy_msg_rx.frame_fields.data_len;

  dbc_decode_SENSOR_SONARS(&sensor_data, sensor_header, dummy_msg_rx.data.bytes);

  can__rx_ExpectAndReturn(can1, &dummy_msg_rx, 0, 1);
  can__rx_IgnoreArg_can_message_ptr();

  can__rx_ReturnThruPtr_can_message_ptr(&dummy_msg_rx);
  can__rx_ExpectAndReturn(can1, &dummy_msg_rx, 0, 0);
  can__rx_IgnoreArg_can_message_ptr();

  // we will not call driver_process_input function as the data_len is incorrect
  can_bus_handler__process_all_received_messages(can1);
}

void test_can_bus_message_handler_rx_geo_success(void) {
  can__msg_t dummy_msg_rx_geo = {0};
  dbc_GEO_CONTROLLER_COMPASS_s geo_status = {0};
  dbc_message_header_t geo_header;

  dbc_decode_GEO_CONTROLLER_COMPASS(&geo_status, geo_header, dummy_msg_rx_geo.data.bytes);
  dummy_msg_rx_geo.msg_id = dbc_header_GEO_CONTROLLER_COMPASS.message_id;
  dummy_msg_rx_geo.frame_fields.data_len = dbc_header_GEO_CONTROLLER_COMPASS.message_dlc;

  geo_header.message_id = dummy_msg_rx_geo.msg_id;
  geo_header.message_dlc = dummy_msg_rx_geo.frame_fields.data_len;

  can__rx_ExpectAndReturn(can1, &dummy_msg_rx_geo, 0, 1);
  can__rx_IgnoreArg_can_message_ptr();

  can__rx_ReturnThruPtr_can_message_ptr(&dummy_msg_rx_geo);
  can__rx_ExpectAndReturn(can1, &dummy_msg_rx_geo, 0, 0);
  can__rx_IgnoreArg_can_message_ptr();

  driver__process_geo_controller_directions_Expect(&geo_status);
  can_bus_handler__process_all_received_messages(can1);
}

// incorrect message id & correct data length
void test_can_bus_message_handler_rx_geo_failure_1(void) {
  can__msg_t dummy_msg_rx_geo = {};
  dbc_SENSOR_SONARS_s geo_status = {};
  dbc_message_header_t sensor_header;

  dummy_msg_rx_geo.msg_id = dbc_header_SENSOR_SONARS.message_id - 3;
  dummy_msg_rx_geo.frame_fields.data_len = dbc_header_SENSOR_SONARS.message_dlc;

  sensor_header.message_id = dummy_msg_rx_geo.msg_id;
  sensor_header.message_dlc = dummy_msg_rx_geo.frame_fields.data_len;

  dbc_decode_SENSOR_SONARS(&geo_status, sensor_header, dummy_msg_rx_geo.data.bytes);

  can__rx_ExpectAndReturn(can1, &dummy_msg_rx_geo, 0, 1);
  can__rx_IgnoreArg_can_message_ptr();

  can__rx_ReturnThruPtr_can_message_ptr(&dummy_msg_rx_geo);
  can__rx_ExpectAndReturn(can1, &dummy_msg_rx_geo, 0, 0);
  can__rx_IgnoreArg_can_message_ptr();

  // we will not call driver_process_input function as the msg_id is incorrect
  can_bus_handler__process_all_received_messages(can1);
}

// incorrect message id & correct data length
void test_can_bus_message_handler_rx_geo_failure_2(void) {
  can__msg_t dummy_msg_rx_geo = {};
  dbc_SENSOR_SONARS_s geo_status = {};
  dbc_message_header_t sensor_header;

  dummy_msg_rx_geo.msg_id = dbc_header_SENSOR_SONARS.message_id;
  dummy_msg_rx_geo.frame_fields.data_len = dbc_header_SENSOR_SONARS.message_dlc - 2;

  sensor_header.message_id = dummy_msg_rx_geo.msg_id;
  sensor_header.message_dlc = dummy_msg_rx_geo.frame_fields.data_len;

  dbc_decode_SENSOR_SONARS(&geo_status, sensor_header, dummy_msg_rx_geo.data.bytes);

  can__rx_ExpectAndReturn(can1, &dummy_msg_rx_geo, 0, 1);
  can__rx_IgnoreArg_can_message_ptr();

  can__rx_ReturnThruPtr_can_message_ptr(&dummy_msg_rx_geo);
  can__rx_ExpectAndReturn(can1, &dummy_msg_rx_geo, 0, 0);
  can__rx_IgnoreArg_can_message_ptr();

  // we will not call driver_process_input function as the msg_id is incorrect
  can_bus_handler__process_all_received_messages(can1);
}