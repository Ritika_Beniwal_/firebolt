#include "driver_logic.h"
#include "head_tail_lights.h"
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

static dbc_SENSOR_SONARS_s internal_sensor_data = {0};
static dbc_DRIVER_TO_MOTOR_s motor_signal = {0};
static dbc_DRIVER_DEBUG_s debug_values = {0};
static dbc_GEO_CONTROLLER_COMPASS_s geo_data = {0};
static dbc_MOTOR_SPEED_s car_speed = {.MOTOR_SPEED_info = 0};
uint8_t start_driving = 0;
uint8_t start_sig = 0;
static const uint16_t sonar_sensor_min_limit = 60; // cm
static const uint16_t front_sonar_sensor_min_limit = 70;
static const uint16_t rear_sonar_sensor_min_limit = 40;
static const int max_angle_threshold = 45; // degrees
static const int offset_to_angle = 5;

static uint32_t distance_to_dest = 0;
const static uint32_t car_stop_threshold_distance = 5;
static bool destination_reached_flag = false;
static direction_t gps_navigation_direction;
static float heading_difference = 0;

const static float reverse_speed = -4;
const static float ideal_speed = 5.5;
const static float slow_speed = 5;

/**************************************** Process Ultrasonic Sensor Data
 * *************************************************/
void driver_process_sonar_sensor_input(dbc_SENSOR_SONARS_s *sensor_data) {
  if (sensor_data == NULL) {
    return;
  }
  internal_sensor_data.SENSOR_SONARS_left = sensor_data->SENSOR_SONARS_left;
  internal_sensor_data.SENSOR_SONARS_right = sensor_data->SENSOR_SONARS_right;
  internal_sensor_data.SENSOR_SONARS_middle = sensor_data->SENSOR_SONARS_middle;
  internal_sensor_data.SENSOR_SONARS_rear = sensor_data->SENSOR_SONARS_rear;

  fprintf(stderr, "l: %d, r: %d, m: %d, b: %d\n", internal_sensor_data.SENSOR_SONARS_left,
          internal_sensor_data.SENSOR_SONARS_right, internal_sensor_data.SENSOR_SONARS_middle,
          internal_sensor_data.SENSOR_SONARS_rear);
}

/************************************* Process Geo Data **************************************************************/
void driver__process_geo_controller_directions(dbc_GEO_CONTROLLER_COMPASS_s *geo_status) {
  if (geo_status == NULL) {
    return;
  }
  geo_data.HEADING = geo_status->HEADING;
  geo_data.BEARING = geo_status->BEARING;
  geo_data.DISTANCE_TO_DESTINATION = geo_status->DISTANCE_TO_DESTINATION;
  distance_to_dest = geo_data.DISTANCE_TO_DESTINATION;
  fprintf(stderr, "h: %f, b: %f/, dist: %f\n", geo_data.HEADING, geo_data.BEARING, geo_data.DISTANCE_TO_DESTINATION);
}

/************************************* Process RPM Sensor Data
 * **************************************************************/
void driver__process_rpm_values(dbc_MOTOR_SPEED_s *rpm_sensor) {
  car_speed.MOTOR_SPEED_info = rpm_sensor->MOTOR_SPEED_info;
  fprintf(stderr, "rpm: %0.1f\n", car_speed);
}

void driver__process_start_stop_signal(dbc_DRIVE_STATUS_s *start_signal) {
  start_sig = start_signal->DRIVE_START_STOP;
  if (start_sig == 1) {
    fprintf(stderr, "signal processing %d\n", start_sig);
    start_driving = true;
  } else {
    start_driving = false;
  }
}

static bool is_obstacle_detected(void) {
  if (internal_sensor_data.SENSOR_SONARS_left <= sonar_sensor_min_limit ||
      internal_sensor_data.SENSOR_SONARS_middle <= front_sonar_sensor_min_limit ||
      internal_sensor_data.SENSOR_SONARS_right <= sonar_sensor_min_limit) {
    return true;
  } else {
    return false;
  }
}

static bool obstacle_on_all_front_sides() {
  if (internal_sensor_data.SENSOR_SONARS_left <= sonar_sensor_min_limit &&
      internal_sensor_data.SENSOR_SONARS_middle <= front_sonar_sensor_min_limit &&
      internal_sensor_data.SENSOR_SONARS_right <= sonar_sensor_min_limit) {
    return true;
  } else
    return false;
}

static bool obstacle_on_left() {
  if (internal_sensor_data.SENSOR_SONARS_left <= sonar_sensor_min_limit) {
    return true;
  } else
    return false;
}

static bool obstacle_in_right() {
  if (internal_sensor_data.SENSOR_SONARS_right <= sonar_sensor_min_limit) {
    return true;
  } else
    return false;
}

static bool obstacle_on_rear() {
  if (internal_sensor_data.SENSOR_SONARS_rear <= rear_sonar_sensor_min_limit)
    return true;
  else
    return false;
}

static bool obstacle_on_front() {
  if (internal_sensor_data.SENSOR_SONARS_middle <= front_sonar_sensor_min_limit) {
    return true;
  } else
    return false;
}

static void stop_the_car() {
  motor_signal.DRIVER_TO_MOTOR_direction = 0;
  motor_signal.DRIVER_TO_MOTOR_speed = 0;
  set_taillights();
  clear_headlights();
}

/******************************************* Reverse Car ***************************************/
static void reverse_car_and_steer(void) {
  if (!obstacle_on_rear()) {
    // motor_signal.DRIVER_TO_MOTOR_direction = 0;
    motor_signal.DRIVER_TO_MOTOR_direction = (motor_signal.DRIVER_TO_MOTOR_direction <= 35)
                                                 ? motor_signal.DRIVER_TO_MOTOR_direction + 10
                                                 : max_angle_threshold;
    motor_signal.DRIVER_TO_MOTOR_speed = reverse_speed;
    update_lights(10, taillight_left);
    update_lights(10, taillight_right);
  } else {
    stop_the_car();
  }
}

static void get_steering_range(bool obstactle_on_right) {
  if (obstactle_on_right == true) {
    // motor_signal.DRIVER_TO_MOTOR_direction = MOTOR_direction_HARD_LEFT;
    motor_signal.DRIVER_TO_MOTOR_direction = (motor_signal.DRIVER_TO_MOTOR_direction <= 40)
                                                 ? motor_signal.DRIVER_TO_MOTOR_direction + offset_to_angle
                                                 : max_angle_threshold;
    motor_signal.DRIVER_TO_MOTOR_speed = slow_speed;
    update_lights(10, headlight_left);

  } else {
    //    motor_signal.DRIVER_TO_MOTOR_direction = MOTOR_direction_HARD_RIGHT;

    motor_signal.DRIVER_TO_MOTOR_direction = (motor_signal.DRIVER_TO_MOTOR_direction >= -40)
                                                 ? motor_signal.DRIVER_TO_MOTOR_direction - offset_to_angle
                                                 : -max_angle_threshold;

    motor_signal.DRIVER_TO_MOTOR_speed = slow_speed;
    update_lights(10, headlight_right);
  }
}

static void drive_forward(void) {
  motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
  motor_signal.DRIVER_TO_MOTOR_direction = 0;
  set_headlights();
  set_taillights();
}

bool destination_reached() {
  bool return_val = false;
  if (distance_to_dest <= car_stop_threshold_distance) {
    gpio__set(taillight_left);
    gpio__set(taillight_left);
    gpio__set(taillight_left);
    gpio__set(taillight_left);
    return return_val;
  }

  else
    return return_val;
}

/********************************* Steering and Speed commands to motor
 * **********************************************************/
dbc_DRIVER_TO_MOTOR_s send_info_to_motor(direction_t direction_to_turn, float turn_magnitude, float dist_magnitude) {
  // Direction to the right or left just means your angle change will be positive or negative
  switch (direction_to_turn) {
  case right:
    /*if (direction_to_turn == right) {*/
    if (turn_magnitude / 4 >= 40) {
      motor_signal.DRIVER_TO_MOTOR_direction = -40;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRHRD\n");
    } else if (turn_magnitude / 4 >= 35) {
      motor_signal.DRIVER_TO_MOTOR_direction = -35;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRHRD\n");
    } else if (turn_magnitude / 4 >= 30) {
      motor_signal.DRIVER_TO_MOTOR_direction = -30;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRHRD\n");
    } else if (turn_magnitude / 4 >= 25) {
      motor_signal.DRIVER_TO_MOTOR_direction = -25;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRHRD\n");
    } else if (turn_magnitude / 4 >= 20) {
      motor_signal.DRIVER_TO_MOTOR_direction = -20;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRSFT\n");
    } else if (turn_magnitude / 4 >= 15) {
      motor_signal.DRIVER_TO_MOTOR_direction = -15;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRSFT\n");
    } else if (turn_magnitude / 4 >= 10) {
      motor_signal.DRIVER_TO_MOTOR_direction = -10;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRSFT\n");
    } else if (turn_magnitude / 4 >= 5) {
      motor_signal.DRIVER_TO_MOTOR_direction = -5;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NRSFT\n");
    } else {
    }
    break;
    /*}*/

    /*else if (direction_to_turn == left) {*/
  // Do the same thing but the angles are positive
  case left:
    if (turn_magnitude / 4 >= 40) {
      motor_signal.DRIVER_TO_MOTOR_direction = 40;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLHRD\n");
    } else if (turn_magnitude / 4 >= 35) {
      motor_signal.DRIVER_TO_MOTOR_direction = 35;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLHRD\n");
    } else if (turn_magnitude / 4 >= 30) {
      motor_signal.DRIVER_TO_MOTOR_direction = 30;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLHRD\n");
    } else if (turn_magnitude / 4 >= 25) {
      motor_signal.DRIVER_TO_MOTOR_direction = 25;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLHRD\n");
    } else if (turn_magnitude / 4 >= 20) {
      motor_signal.DRIVER_TO_MOTOR_direction = 20;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLSFT\n");
    } else if (turn_magnitude / 4 >= 15) {
      motor_signal.DRIVER_TO_MOTOR_direction = 15;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLSFT\n");
    } else if (turn_magnitude / 4 >= 10) {
      motor_signal.DRIVER_TO_MOTOR_direction = 10;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLSFT\n");
    } else if (turn_magnitude / 4 >= 5) {
      motor_signal.DRIVER_TO_MOTOR_direction = 5;
      motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
      fprintf(stderr, "NLSFT\n");
    } else {
    }
    break;
    /*}*/

  case straight:
    /*else if (direction_to_turn == straight) {*/
    motor_signal.DRIVER_TO_MOTOR_direction = 0;
    motor_signal.DRIVER_TO_MOTOR_speed = ideal_speed;
    /*}*/
    break;
  }

  return motor_signal;
}
/*********************************************************************************************************************/

/*********************************** Decide Navigation direction *****************************************************/
void navigate_towards_destination() {
  fprintf(stderr, "Navigate\n");
  gps_navigation_direction = right;

  heading_difference = geo_data.BEARING - geo_data.HEADING;

  if (heading_difference >= 350 && heading_difference <= 10) {
    gps_navigation_direction = straight;
    heading_difference = fabs(heading_difference);

    debug_values.car_driving_status = FORWARD;
    debug_values.car_steering_status = STRAIGHT;
  } else if (heading_difference > 180) {
    gps_navigation_direction = left;
    heading_difference = 360 - heading_difference;

    debug_values.car_driving_status = FORWARD;
    debug_values.car_steering_status = LEFT;

  } else if (heading_difference < 0 && heading_difference > -180) {
    gps_navigation_direction = left;
    heading_difference = fabs(heading_difference);

    debug_values.car_driving_status = FORWARD;
    debug_values.car_steering_status = LEFT;
  }

  else if (heading_difference < -180) {
    gps_navigation_direction = right;
    heading_difference = fabs(heading_difference + 360);

    debug_values.car_driving_status = FORWARD;
    debug_values.car_steering_status = RIGHT;
  } else if (heading_difference > 0 && heading_difference <= 180) {
    gps_navigation_direction = right;
    debug_values.car_driving_status = FORWARD;
    debug_values.car_steering_status = RIGHT;
  }

  send_info_to_motor(gps_navigation_direction, heading_difference, geo_data.DISTANCE_TO_DESTINATION);
}
/********************************************************************************************************************/

/************************************************* Motor Commands from Driver Node *********************************/
dbc_DRIVER_TO_MOTOR_s driver_logic__get_motor_command(void) {
  if (start_driving == 1) {
    car_start_lights_pattern();
    bool obstacle_on_right = false;
    if (distance_to_dest > car_stop_threshold_distance) {
      if (is_obstacle_detected()) {
        if (obstacle_on_all_front_sides()) {
          stop_the_car();
          reverse_car_and_steer();
        } else if (obstacle_on_left() && obstacle_in_right() && (!obstacle_on_front())) {
          drive_forward();
        } else if (obstacle_on_left() && (!obstacle_in_right())) {
          obstacle_on_right = false;
          get_steering_range(obstacle_on_right); // right steer
        } else if (obstacle_in_right() && (!obstacle_on_left())) {
          obstacle_on_right = true;
          get_steering_range(obstacle_on_right); // left steer
        } else if (obstacle_on_front() && (!obstacle_on_left() && !obstacle_in_right())) {
          stop_the_car();
          reverse_car_and_steer();
          obstacle_on_right =
              (internal_sensor_data.SENSOR_SONARS_right < internal_sensor_data.SENSOR_SONARS_left) ? true : false;
          get_steering_range(obstacle_on_right);

        } else if (obstacle_on_rear() && (!obstacle_on_all_front_sides())) {
          obstacle_on_right =
              (internal_sensor_data.SENSOR_SONARS_right < internal_sensor_data.SENSOR_SONARS_left) ? true : false;
          get_steering_range(obstacle_on_right);
          debug_values.car_driving_status = FORWARD;
          debug_values.car_steering_status = STRAIGHT;
        } else {
          stop_the_car();

          debug_values.car_driving_status = STOPPED;
          debug_values.car_steering_status = STRAIGHT;
        }
      } else {
        navigate_towards_destination();
        // drive_forward();
      }
    } else {
      stop_the_car();
      fprintf(stderr, "No Navigation coordinates received\n");
    }

  } else {
    stop_the_car();
    clear_headlights();
    clear_taillights();
    fprintf(stderr, "Awaiting Start Signal from App\n");
  }
  return motor_signal;
}
/**************************************************************************************************/

/****************************************** Debug Values *****************************************/
uint8_t get_driving_status() { return debug_values.car_driving_status; }

uint8_t get_steering_status() { return debug_values.car_steering_status; }

dbc_DRIVER_DEBUG_s driver_process_debug_data(void) {
  dbc_DRIVER_DEBUG_s driver_debug_data;
  driver_debug_data.car_driving_status = get_driving_status();
  driver_debug_data.car_steering_status = get_steering_status();
  return driver_debug_data;
}

/***************************************************************************************************/

/*************************************** LCD Printing Data ******************************************/
uint32_t get_distance_to_dest() { return geo_data.DISTANCE_TO_DESTINATION; }

uint16_t get_heading_from_geo() { return geo_data.HEADING; }

uint16_t get_bearing_from_geo() { return geo_data.BEARING; }

double get_car_speed() { return car_speed.MOTOR_SPEED_info; }

dbc_SENSOR_SONARS_s get_sensor_values_for_lcd() { return internal_sensor_data; }

/********************************************************************************************/

//+45--> left
//-45--> right