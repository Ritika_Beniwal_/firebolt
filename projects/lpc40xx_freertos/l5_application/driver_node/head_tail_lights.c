#include "head_tail_lights.h"

void head_tail_lights_init(void) {
  gpio__construct_as_output(headlight_left.port_number, headlight_left.pin_number);
  gpio__construct_as_output(headlight_right.port_number, headlight_right.pin_number);
  gpio__construct_as_output(taillight_right.port_number, taillight_right.pin_number);
  gpio__construct_as_output(taillight_left.port_number, taillight_left.pin_number);
}

void clear_headlights(void) {
  gpio__set(headlight_left);
  gpio__set(headlight_right);
}

void clear_taillights(void) {
  gpio__set(taillight_left);
  gpio__set(taillight_right);
}

void set_headlights(void) {
  gpio__reset(headlight_left);
  gpio__reset(headlight_right);
}

void set_taillights(void) {
  gpio__reset(taillight_left);
  gpio__reset(taillight_right);
}

void update_lights(uint32_t callback_count, gpio_s gpio_parameters) {
  if ((callback_count % 2) == 0) {
    gpio__toggle(gpio_parameters);
    gpio__toggle(gpio_parameters);
  } else {
    gpio__set(gpio_parameters);
    gpio__set(gpio_parameters);
  }
}

void car_start_lights_pattern(void) {
  clear_headlights();
  clear_taillights();
  set_headlights();
  set_taillights();
  clear_headlights();
  clear_taillights();
}