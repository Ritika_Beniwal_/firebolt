#pragma once

#include "board_io.h"
#include "gpio.h"

static const gpio_s headlight_left = {.port_number = 2, .pin_number = 1};
static const gpio_s headlight_right = {.port_number = 2, .pin_number = 4};
static const gpio_s taillight_left = {.port_number = 2, .pin_number = 6};
static const gpio_s taillight_right = {.port_number = 2, .pin_number = 8};

void head_tail_lights_init(void);

void clear_headlights(void);

void clear_taillights(void);

void set_headlights(void);

void set_taillights(void);

void update_lights(uint32_t callback_count, gpio_s gpio_parameters);

void car_start_lights_pattern(void);