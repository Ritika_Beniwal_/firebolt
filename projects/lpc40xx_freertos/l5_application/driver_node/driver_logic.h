#pragma once
#include "project.h"

typedef enum { right = 0, left = 1, straight = 2 } direction_t;

// This should copy the sensor data to its internal "static" struct instance of dbc_SENSOR_SONARS_s
void driver_process_sonar_sensor_input(dbc_SENSOR_SONARS_s *sensor_data);

// This should copy the geo data to its internal "static" struct instance of dbc_GEO_CONTROLLER_COMPASS_s
void driver__process_geo_controller_directions(dbc_GEO_CONTROLLER_COMPASS_s *geo_status);

// This should operate on the "static" struct instance of dbc_SENSOR_SONARS_s to run the OAL
dbc_DRIVER_TO_MOTOR_s driver_logic__get_motor_command(void);

void driver__process_rpm_values(dbc_MOTOR_SPEED_s *rpm_value);

void driver__process_start_stop_signal(dbc_DRIVE_STATUS_s *start_signal);

uint32_t get_distance_to_dest();
uint16_t get_heading_from_geo();
double get_car_speed();
uint16_t get_bearing_from_geo();
bool destination_reached();

dbc_SENSOR_SONARS_s get_sensor_values_for_lcd();
dbc_DRIVER_DEBUG_s driver_process_debug_data(void);