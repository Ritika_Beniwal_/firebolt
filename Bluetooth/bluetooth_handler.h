#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

// Initializes UART, BT peripheral, GPIO, and Line Buffer
bool bluetooth_connector__init(void);

// Accepts a string input, and sends it over UART to the BT peripheral
// Tries to write a full line to the uart driver/bt peripheral
void bluetooth_connector__transmit_message(char *message);

// Reads from UART everything it can, and saves the data in the internal line buffer
void bluetooth_connector__receive_data(void);

// Tries to extract a full line from the internal line_buffer
bool bluetooth_connector__extract_message(char *message_buffer, size_t buffer_size);