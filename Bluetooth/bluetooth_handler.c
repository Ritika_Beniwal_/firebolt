#include "bluetooth_handler.h"
#include <stdio.h>

#include "clock.h"
#include "gpio.h"
#include "line_buffer.h"
#include "uart.h"
#include "uart_printf.h"

static const uart_e BLUETOOTH_UART = UART__3;
static const uint32_t SJ2_CLOCK_HZ = 384000000;
static const uint32_t BLUETOOTH_BAUD_RATE = 38400;
static const uint32_t UART_RX_QUEUE_SIZE = 100;
static const uint32_t UART_TX_QUEUE_SIZE = 100;
static const uint32_t ZERO_TIMEOUT = 0;

static char buffer_memory[200];
static line_buffer_s line_buffer;

bool bluetooth_connector__init(void) {
  /* Initialize UART*/
  uart__init(BLUETOOTH_UART, SJ2_CLOCK_HZ, BLUETOOTH_BAUD_RATE);
  QueueHandle_t rxq_handle = xQueueCreate(UART_RX_QUEUE_SIZE, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(UART_TX_QUEUE_SIZE, sizeof(char));
  bool init_success = uart__enable_queues(BLUETOOTH_UART, rxq_handle, txq_handle);

  /*Initialize GPIO Pins*/
  if (init_success) {
    gpio_s uart3_tx = gpio__construct(GPIO__PORT_4, 28); // P4.28 - Uart-3 Tx
    gpio_s uart3_rx = gpio__construct(GPIO__PORT_4, 29); // P4.29 - Uart-3 Rx
    gpio__set_as_output(uart3_tx);
    gpio__set_as_input(uart3_rx);
    gpio__set_function(uart3_tx, GPIO__FUNCTION_2);
    gpio__set_function(uart3_rx, GPIO__FUNCTION_2);

    /*
     *Initialize line buffer
     */
    init_success = line_buffer__init(&line_buffer, buffer_memory, sizeof(buffer_memory));
  }

  return init_success;
}

void bluetooth_connector__transmit_message(char *message) { uart_printf(BLUETOOTH_UART, "%s", message); }

void bluetooth_connector__receive_data(void) {
  char byte = 0;
  while (uart__get(BLUETOOTH_UART, &byte, ZERO_TIMEOUT)) {
    line_buffer__add_byte(&line_buffer, byte);
  }
}

bool bluetooth_connector__extract_message(char *message_buffer, size_t buffer_size) {
  return line_buffer__remove_line(&line_buffer, message_buffer, buffer_size);
}