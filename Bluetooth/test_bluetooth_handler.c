#include <string.h>

#include "unity.h"

#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockqueue.h"
#include "Mockuart.h"
#include "Mockuart_printf.h"

#include "line_buffer.h"

#include "bluetooth_handler.h"

static void mock_receive_string_from_uart(char *uart_driver_returned_data) {
  for (size_t index = 0; index < strlen(uart_driver_returned_data); index++) {
    uart__get_ExpectAndReturn(UART__3, NULL, 0, true);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&(uart_driver_returned_data[index]));
  }
  uart__get_ExpectAndReturn(UART__3, NULL, 0, false);
  uart__get_IgnoreArg_input_byte();
}

/***********************************************/

void setUp(void) {
  uart__init_Expect(UART__3, 384000000, 38400);
  xQueueCreate_ExpectAndReturn(100, 1, NULL);
  xQueueCreate_ExpectAndReturn(100, 1, NULL);
  uart__enable_queues_ExpectAnyArgsAndReturn(true);

  gpio_s uart3_tx = {0};
  gpio_s uart3_rx = {0};
  gpio__construct_ExpectAnyArgsAndReturn(uart3_tx);
  gpio__construct_ExpectAnyArgsAndReturn(uart3_rx);
  gpio__set_as_output_Expect(uart3_tx);
  gpio__set_as_input_Expect(uart3_rx);
  gpio__set_function_Expect(uart3_tx, GPIO__FUNCTION_2);
  gpio__set_function_Expect(uart3_rx, GPIO__FUNCTION_2);

  TEST_ASSERT_TRUE(bluetooth_connector__init());
}

void tearDown(void) {}

void test_bluetooth_connector__init(void) {
  uart__init_Expect(UART__3, 384000000, 38400);
  xQueueCreate_ExpectAndReturn(100, 1, NULL);
  xQueueCreate_ExpectAndReturn(100, 1, NULL);
  uart__enable_queues_ExpectAnyArgsAndReturn(true);

  gpio_s uart3_tx = {0};
  gpio_s uart3_rx = {0};
  gpio_s uart3_cts = {0};
  gpio__construct_ExpectAnyArgsAndReturn(uart3_tx);
  gpio__construct_ExpectAnyArgsAndReturn(uart3_rx);
  gpio__set_as_output_Expect(uart3_tx);
  gpio__set_as_input_Expect(uart3_rx);
  gpio__set_function_Expect(uart3_tx, GPIO__FUNCTION_2);
  gpio__set_function_Expect(uart3_rx, GPIO__FUNCTION_2);

  TEST_ASSERT_TRUE(bluetooth_connector__init());
}

void test_bluetooth_connector__init_uart_error(void) {
  uart__init_Expect(UART__3, 384000000, 38400);
  xQueueCreate_ExpectAndReturn(100, 1, NULL);
  xQueueCreate_ExpectAndReturn(100, 1, NULL);
  uart__enable_queues_ExpectAnyArgsAndReturn(false);

  TEST_ASSERT_FALSE(bluetooth_connector__init());
}

void test_bluetooth_connector__transmit_message(void) {
  char message[10] = "abcde\n";
  uart_printf_ExpectAnyArgsAndReturn(strlen(message));

  bluetooth_connector__transmit_message(message);
}

void test_bluetooth_connector__receive_data(void) {
  char byte = 'a';
  uart__get_ExpectAndReturn(UART__3, NULL, 0, true);
  uart__get_IgnoreArg_input_byte();
  uart__get_ReturnThruPtr_input_byte(&byte);

  uart__get_ExpectAnyArgsAndReturn(false);

  bluetooth_connector__receive_data();
}

void test_bluetooth_connector__extract_message(void) {
  // First receive a full message from UART
  char *uart_driver_returned_data = "abcdef\n";
  mock_receive_string_from_uart(uart_driver_returned_data);
  bluetooth_connector__receive_data();

  // Then extract the message as a full line

  char message_buffer[20] = {0};
  TEST_ASSERT_TRUE(bluetooth_connector__extract_message(message_buffer, sizeof(message_buffer)));
  TEST_ASSERT_EQUAL_STRING("abcdef", message_buffer);
}