#include <string.h>

#include "line_buffer.h"

static bool line_buffer__private_contains_newline(line_buffer_s *buffer) {
  return strchr(buffer->memory, '\n') != NULL;
}

static char *line_buffer__private_find_newline(line_buffer_s *buffer) { return strchr(buffer->memory, '\n'); }

static bool line_buffer__private_is_full(line_buffer_s *buffer) { return buffer->write_index >= buffer->max_data_size; }

/***********************************************/

bool line_buffer__init(line_buffer_s *buffer, char *memory, size_t size_of_memory) {
  bool buffer_initialized_successfully = false;

  if (buffer != NULL && memory != NULL && size_of_memory > 1) {
    memset(memory, 0, size_of_memory);
    buffer->memory = memory;
    buffer->memory_size = size_of_memory;
    buffer->max_data_size = size_of_memory - 1; // leave null-terminating char at the end of memory
    buffer->write_index = 0;
    buffer_initialized_successfully = true;
  }

  return buffer_initialized_successfully;
}

bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {
  bool byte_added_successfully = false;
  if (buffer != NULL && !line_buffer__private_is_full(buffer)) {
    buffer->memory[buffer->write_index] = byte;
    buffer->write_index++;
    byte_added_successfully = true;
  }

  return byte_added_successfully;
}

bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {
  bool line_removed_successfully = false;

  if (buffer != NULL && line != NULL && line_max_size > 1) {
    size_t line_max_data_size = line_max_size - 1; // enforce null-terminating char at the end of 'line'

    if (line_buffer__private_contains_newline(buffer)) {
      char *newline_ptr = line_buffer__private_find_newline(buffer);
      size_t num_bytes_to_copy_line = newline_ptr - &(buffer->memory[0]);

      if (line_max_data_size >= num_bytes_to_copy_line) {
        // Extract the line to return
        memset(line, 0, line_max_size);
        memcpy(line, &(buffer->memory[0]), num_bytes_to_copy_line);

        // Shift over the rest of the data buffer
        size_t num_bytes_to_shift_buffer = &(buffer->memory[buffer->write_index]) - (newline_ptr + 1);
        memcpy(&(buffer->memory[0]), newline_ptr + 1, num_bytes_to_shift_buffer);
        buffer->write_index -= (num_bytes_to_copy_line + 1);

        // Clear out the memory of the shifted data
        memset(&(buffer->memory[buffer->write_index]), 0, num_bytes_to_copy_line + 1);
        line_removed_successfully = true;
      }

    } else if (line_buffer__private_is_full(buffer) && line_max_data_size >= buffer->max_data_size) {
      memset(line, 0, line_max_size);
      memcpy(line, &(buffer->memory[0]), buffer->max_data_size);
      buffer->write_index = 0;
      line_removed_successfully = true;
    }
  }

  return line_removed_successfully;
}