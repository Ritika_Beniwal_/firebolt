#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * The line buffer module uses the user-provided memory block to store data formatted as strings.
 * The module tracks the size of the memory block, and reserves the last byte of the memory as
 * a dedicated null-terminating character. This facilitates safer string operations that assume
 * the data always has a terminating null-character.
 */
typedef struct {
  char *memory;
  size_t memory_size;
  size_t max_data_size; // limit the modifiable memory to ensure null-terminating char
  size_t write_index;
} line_buffer_s;

/**
 * Initialize *line_buffer_s with the user provided buffer space and size
 * User should initialize the buffer with whatever memory they need.
 * The memory size must be greater than 1 byte.
 * @code
 *  char memory[256];
 *  line_buffer_s line_buffer = { };
 *  line_buffer__init(&line_buffer, memory, sizeof(memory));
 * @endcode
 */
bool line_buffer__init(line_buffer_s *buffer, char *memory, size_t size_of_memory);

// Adds a byte to the buffer, and returns true if the buffer had enough space to add the byte
bool line_buffer__add_byte(line_buffer_s *buffer, char byte);

/**
 * If the line buffer has a complete line, it will remove that contents and save it to "char * line"
 * Note that the buffer may have multiple lines already in the buffer, so it will require multiple
 * calls to this function to empty out those lines.
 *
 * The one corner case is that if the buffer is FULL, and there is no '\n' character, then you should
 * empty out the line to the user buffer even though there is no newline character
 *
 * The line param size must be greater than 1 byte.
 *
 * @param line_max_size This is the max size of 'char * line' memory pointer
 *
 * Returns false if the next available line in the buffer is longer than "line_max_size"
 */
bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size);