#include <string.h>

#include "unity.h"

// Include the source we wish to test
// Include "xxx.c" to get access to static functions
#include "line_buffer.c"

static line_buffer_s line_buffer;
static char memory[10]; // stores max 9-char string, followed by null-term char
static const size_t MEMORY_DATA_SIZE = sizeof(memory) - 1;

static void add_to_line_buffer(const char *string) {
  for (size_t index = 0; index < strlen(string); index++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, string[index]));
  }
}

static void fill_buffer_completely_no_newline() {
  char byte_to_add;
  int filler_char_range = 26;
  for (size_t index = 0; index < MEMORY_DATA_SIZE; index++) {
    byte_to_add = 'a' + (index % filler_char_range);
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, byte_to_add));
  }

  TEST_ASSERT_FALSE(line_buffer__add_byte(&line_buffer, 'a'));
}

/***********************************************/

void setUp(void) { TEST_ASSERT_TRUE(line_buffer__init(&line_buffer, memory, sizeof(memory))); }

void test_line_buffer__init(void) { TEST_ASSERT_TRUE(line_buffer__init(&line_buffer, memory, sizeof(memory))); }

void test_line_buffer__init_fail_null_buffer(void) {
  TEST_ASSERT_FALSE(line_buffer__init(NULL, memory, sizeof(memory)));
}

void test_line_buffer__init_fail_null_memory(void) {
  TEST_ASSERT_FALSE(line_buffer__init(&line_buffer, NULL, sizeof(memory)));
}

void test_line_buffer__init_fail_memory_size_too_small(void) {
  TEST_ASSERT_FALSE(line_buffer__init(&line_buffer, NULL, 0));
  TEST_ASSERT_FALSE(line_buffer__init(&line_buffer, NULL, 1));
}

void test_line_buffer__add_byte_empty(void) {
  char byte_to_add = 'a';
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, byte_to_add));
}

void test_line_buffer__add_byte_full(void) {
  fill_buffer_completely_no_newline();

  // Should not be able to add any more bytes
  TEST_ASSERT_FALSE(line_buffer__add_byte(&line_buffer, 'a'));
}

void test_line_buffer__add_byte_null_buffer(void) {
  char byte_to_add = 'a';
  TEST_ASSERT_FALSE(line_buffer__add_byte(NULL, byte_to_add));
}

void test_line_buffer__add_line(void) { add_to_line_buffer("abc\n"); }

void test_line_buffer__remove_line_nominal(void) {
  add_to_line_buffer("abc\n");

  char removed_line[20] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, removed_line, sizeof(removed_line)));
  TEST_ASSERT_EQUAL_STRING("abc", removed_line);
}

void test_line_buffer__remove_line_multiple_lines(void) {
  add_to_line_buffer("abc\n");
  add_to_line_buffer("ddd\n");

  char removed_line[20] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, removed_line, sizeof(removed_line)));
  TEST_ASSERT_EQUAL_STRING("abc", removed_line);
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, removed_line, sizeof(removed_line)));
  TEST_ASSERT_EQUAL_STRING("ddd", removed_line);
}

void test_line_buffer__remove_line_crlf(void) {
  add_to_line_buffer("eee\r\n");

  char removed_line[20] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, removed_line, sizeof(removed_line)));
  TEST_ASSERT_EQUAL_STRING("eee\r", removed_line);
}

void test_line_buffer__remove_line_buffer_overflow_incomplete_line(void) {
  // Fill up the buffer without completing a full line
  fill_buffer_completely_no_newline();

  TEST_ASSERT_TRUE(line_buffer__private_is_full(&line_buffer));
  TEST_ASSERT_FALSE(line_buffer__private_contains_newline(&line_buffer));

  char removed_line[20] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, removed_line, sizeof(removed_line)));
  TEST_ASSERT_EQUAL(MEMORY_DATA_SIZE, strlen(removed_line));
  TEST_ASSERT_EQUAL_STRING("abcdefghi", removed_line); // All chars from the full buffer

  add_to_line_buffer("newstr\n");
}

void test_line_buffer__remove_line_longer_than_line_param_size(void) {
  add_to_line_buffer("abcdefgh\n");

  char short_removed_line[4] = {0};
  TEST_ASSERT_FALSE(line_buffer__remove_line(&line_buffer, short_removed_line, sizeof(short_removed_line)));
}

void test_line_buffer__remove_line_empty(void) {
  char removed_line[20] = {0};
  TEST_ASSERT_FALSE(line_buffer__remove_line(&line_buffer, removed_line, sizeof(removed_line)));
}

void test_line_buffer__remove_line_not_full_incomplete_line(void) {
  add_to_line_buffer("abc");

  char removed_line[20] = {0};
  TEST_ASSERT_FALSE(line_buffer__remove_line(&line_buffer, removed_line, sizeof(removed_line)));
}

void test_line_buffer__remove_line_null_buffer(void) {
  char removed_line[20] = {0};
  TEST_ASSERT_FALSE(line_buffer__remove_line(NULL, removed_line, sizeof(removed_line)));
}

void test_line_buffer__remove_line_null_line_param(void) {
  TEST_ASSERT_FALSE(line_buffer__remove_line(&line_buffer, NULL, 10));
}

void test_line_buffer__remove_line_fail_line_param_size_too_small(void) {
  char removed_line[20] = {0};
  TEST_ASSERT_FALSE(line_buffer__remove_line(&line_buffer, removed_line, 0));
  TEST_ASSERT_FALSE(line_buffer__remove_line(&line_buffer, removed_line, 1));
}

void test_line_buffer__add_and_remove_many_lines(void) {
  size_t num_iterations = 100;
  for (size_t iteration = 0; iteration < num_iterations; iteration++) {
    add_to_line_buffer("ab1\n");
    add_to_line_buffer("g2\n");

    char line[20] = {0};
    TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
    TEST_ASSERT_EQUAL_STRING("ab1", line);

    add_to_line_buffer("tst3\n");

    TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
    TEST_ASSERT_EQUAL_STRING("g2", line);

    TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
    TEST_ASSERT_EQUAL_STRING("tst3", line);

    fill_buffer_completely_no_newline();
    TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
    TEST_ASSERT_EQUAL(MEMORY_DATA_SIZE, strlen(line));

    add_to_line_buffer("aaaaa4\n");
    TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
    TEST_ASSERT_EQUAL_STRING("aaaaa4", line);
  }
}